class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :set_locale
  
  def extract_locale_from_tld
    res = request.host.split(',').last

    res if res != request.host
  end
  
  def extract_locale_from_subdomain
    res = request.subdomains.first 
    
    res if !request.subdomains.empty? and res != request.host
  end
  
  def extract_locale_from_accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end
  
  def set_locale
    extracted_locale = params[:locale] ||
                       session[:locale] ||
                       #extract_locale_from_subdomain ||
                       #extract_locale_from_tld ||
                       extract_locale_from_accept_language_header

    #logger.debug "Locales: #{ [ params[:locale], session[:locale], extract_locale_from_subdomain, extract_locale_from_tld, extract_locale_from_accept_language_header ].each { |l| l.inspect } }"
    logger.debug "Default locale: #{ extracted_locale }"

    I18n.locale = (I18n::available_locales.include? extracted_locale.to_sym) ? extracted_locale :
        I18n.default_locale

    session[:locale] = I18n.locale if params[:locale]
  end
end
